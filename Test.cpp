/*
Created by eugen on 5/28/2017
COMP 5421 :: Assignment 2
Eugen Caruntu #29077103
*/

#include "Command.h"
#include "Led.h"
#include <string>
#include <iostream>

using namespace std;

class Test
{

	void testCommand()
	{
		cout << "========================== testing Command class ==========================" << endl;

		Command c;
		string cmdTests[] = { "1,3r", "p", "=", ",$", "1,n", "10,$n", "1,n", "11a", "1,$n", "18,26c", ".n", ".,.", "", "" , "" , "" , "" , "" , "" , "" , "" , "" , "" };
		for (int i = 0; i < 23; i++)
		{
			cout << endl << "------ [" << cmdTests[i] << "] ---------" << endl;
			c.parse(cmdTests[i], 0, 99); // this will show as 1 to 100

			cout << "---------------------------------------------------------------------------" << endl;
			cout << "Start Range: " + std::to_string(c.fromRng) + " | End Range: " + std::to_string(c.toRng) + " | Command: " + c.symbol << " | Valid: " << valid << endl;
			cout << "---------------------------------------------------------------------------" << endl;

		}

	}

	void testLed()
	{
		cout << "========================== testing Led class ==========================" << endl;
		cout << "";
	}


	int main() // add the arguments as in PDF so it takes command line arguments see driver program item 12 in PDF
	{

		// test Command class
		testCommand();

		// test Command class
		testLed();

		return 0;
	}

};