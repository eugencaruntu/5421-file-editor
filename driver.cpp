// Driver program for assignment 2
// COMP 5421/1 BB, Summer 2017

#include "Command.h"
#include "Led.h"
#include <string>
#include <iostream>

using std::cout;
using std::endl;
using std::string;

int main(int argc, char * argv[]) // add the arguments as in PDF so it takes command line arguments see driver program item 12 in PDF
{
	string filename;				// an empty filename
	switch (argc) {					// determine the filename
	case 1:							// no file name
		break;
	case 2: filename = argv[1];		// initialize filename from argument
		break;
	default: cout << ("too many arguments - all discarded") << endl;
		break;
	}
	Led led(filename);				// create an editor named led
	led.run();						// run our editor

	return 0;
}