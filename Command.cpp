/*
Created by eugen on 5/28/2017
COMP 5421 :: Assignment 2
Eugen Caruntu #29077103
*/
#include <string>
#include <regex>
#include <iostream>
#include "Command.h"

using std::cout;
using std::endl;
using std::string;
using std::regex;

/* default ctor */
Command::Command() {}

/* parse command line */
void Command::parse(const string& cmdLine, const int current_line, const int last_line)
{
	//reset the members
	fromRng = -1;
	toRng = -1;
	valid = false;
	symbol = '\0';

	// incrementing current_line and last_line since list starts from zero
	int crtLine = current_line + 1;
	int lstLine = last_line + 1;

	string cmd = removeSpaces(cmdLine);

	//no command at all means 1d
	if (cmd.length() == 0)
	{
		symbol = 'd';
		fromRng = 1;
		toRng = 1;
		valid = true;
		return;
	} // if we continue means that we have something entered in the command (command string is not empty)

	// '=' does not need anything else
	if (cmd == "=") { symbol = '='; valid = true; return; }

	//the last character if specified, otherwise is a p
	symbol = (isalpha(cmd.back()) || cmd.back() == '=') ? (char)cmd.back() : 'p';

	// fast checks for tables 2 and 3 using regular expressions
	regex r1("(^|\\n)((,\\.|\\.||,|\\.,|\\.,\\.)[pairncw=q])(?=\\n|$)");
	if (regex_match(cmd, r1))
	{
		fromRng = toRng = crtLine;
		valid = true;
		decrement();
		return;
	}
	regex r2("(^|\\n)((,\\.|\\.||,|\\.,|\\.,\\.)[ud])(?=\\n|$)");
	if (regex_match(cmd, r2))
	{
		fromRng = toRng = 1;
		valid = true;
		return; // do not decrement for u, d 
	}
	
	//find last character within available commands validate it
	int optPos = commands.find(symbol);
	if (optPos == -1)
	{
		cout << "Command symbol was not recognised in your entry: [" << cmdLine << "]. Please enter again. \n: " << endl;
		valid = false;
		return;
	};

	// if we are here, the symbol was recognized
	// remove the symbol if present at the end of the command string
	cmd = (cmd.back() == symbol) ? cmd.substr(0, cmd.length() - 1) : cmd;

	// find comma position, will be -1 if not found
	int commaPos = cmd.find_first_of(',');

	/* if comma is found */
	if (commaPos > -1)
	{
		//fromRng will be the first substring up to comma, or set to crtLine if '.' or empty string (comma being first character)
		string startStr = cmd.substr(0, commaPos);
		if (commaPos == 0 || startStr == ".") { fromRng = crtLine; }
		else if (startStr == "$") { fromRng = lstLine; }
		else { fromRng = intConvert(startStr); }

		//toRng will be the substring from comma till the end of the string, or end_line if $ or empty string (comma being last character)
		string endStr = cmd.substr(commaPos + 1, cmd.length() - commaPos);
		if (endStr.length() == 0 || endStr == "$") { toRng = lstLine; }
		else if (endStr == ".") { toRng = crtLine; }
		else { toRng = intConvert(endStr); }
	}

	/* if there is no comma */
	else if (commaPos == -1) // when there is no comma toRng and fromRng are the same
	{
		if ((symbol == 'u' || symbol == 'd') && cmd.length() == 0) { toRng = fromRng = 1; }
		else if (cmd.length() == 0 || cmd == ".") { toRng = fromRng = crtLine; }		// if string length is zero or '.', use crtLine
		else if (cmd == "$") { toRng = fromRng = lstLine; }								// if $ use lstLine
		else { toRng = fromRng = intConvert(cmd); }										// else try to convert to int
	}


	/* check the outcome for fromRng and toRng and invalidate the command if outside bounds */
	if (!(symbol == 'd' || symbol == 'u') && (fromRng < 1 || toRng < 1 || fromRng > toRng || toRng > lstLine))
	{
		cout << "Invalid line address: [" << cmdLine << "]. Please enter again. \n: ";
		valid = false;
		return;
	}

	if ((symbol == 'd' || symbol == 'u') && (fromRng < 1 || toRng < 1))
	{
		cout << "Invalid line address: [" << cmdLine << "]. Please enter again. \n: ";
		valid = false;
		return;
	}

	/* here everything is nice and dandy, validate and decrement to match the list starting at 0 */
	decrement();
	valid = true;
	return; // finally exit
}

/* Removing spaces from a string passed as argument */
string Command::removeSpaces(string str)
{
	return regex_replace(str, regex("\\s+"), "");
}

/* Converting string to integer
Returning int value or -1 if error */
int Command::intConvert(const string& str) const
{
	// check if there are characters other than digits before converting to int (zero length should not be seen here || str.length() == 0 )
	int foundBadChar = str.find_first_not_of("0123456789");
	return (foundBadChar > -1) ? -1 : stoi(str); // will return -1 if conversion was not successfull (string contains other characters than digits, or is empty)
}

void Command::decrement()
{
	/* decrement fromRng so that it matches the list which starts at 0, except for 'u' and 'd' */
	if (!(symbol == 'u' || symbol == 'd')) { --fromRng; --toRng; }
}