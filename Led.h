/*
Created by eugen on 5/28/2017
COMP 5421 :: Assignment 2
Eugen Caruntu #29077103
*/

#ifndef LED_H
#define LED_H
#include "Command.h"
#include <list>
#include <iterator>
#include <string>

using std::list;
using std::iterator;
using std::string;

class Led
{
private:
	int current_line{ 0 };					// current line in document, set to 0 by default (list starts at 0)
	int last_line{ 0 };						// last line of document ($) is set to 0 to properly handle empty documents, constructor will change it if document is loaded
	list<string> buffer;					// empty list of strings 
	list<string>::iterator it = buffer.begin(); // place the iterator at the begining of the list
	list<string>::iterator itEnd = buffer.begin(); // place the second iterator at the begining of the list
	bool saved{ true };						// record the file status (saved = true on open)
	string fileName{ "" };					// file name being used
	bool prtLines{ false };					// used by p and set by n
	void execute(const Command& cmd);		// execute a command object by delegating to appropriate function
	string replace(string& s, const string& a, const string& b); // helper method to do string replacements

	void switchToCommand();
	void advanceIterTo(list<string>:: iterator& iter, const int pos);			// advance iterator to position x if valid

	/*Switches to input mode, reads input lines and appends them to the buffer
	after line y. The current line address is set to last line entered.*/
	void a(const int y);

	/*Switches to input mode, reads input lines and inserts them to the buffer
	before line y. The current line address is set to last line entered.*/
	void i(const int y);

	/*Removes (deletes) the line range x through y from the buffer. If
	there is a line after the deleted line range, then the current line
	address is set to that line. Otherwise the current line address
	is set to the line before the deleted line range.*/
	void r(const int x, const int y);

	/*Prints the line range x through y. The current line address is
	set to the last line printed.*/
	void p(const int x, const int y);

	/*Prints the line range x through y, prexing each line by its line
	number and a tab character. The current line address is set to
	the last line printed.*/
	void n(const int x, const int y);

	/*Prompts for and reads the text to be changed, and then
	prompts for and reads the replacement text. Searches each ad-
	dressed line for an occurrence of the specied string and changes
	all matched strings to the replacement text.*/
	void c(const int x, const int y);

	/*Moves the current line up by y lines, but never beyond the rst
	line, and prints this new current line.*/
	void u(const int y);

	/*Moves the current line down by y lines, but never beyond the
	last line, and prints this new current line.*/
	void d(const int y);

	/*Writes out entire buffer to its associated le. If the buer is
	not associated with a user named le, it prompts for and reads
	the name of the associated file.*/
	void w();

	/*Prints the current line address.*/
	void equal();

	/*quits by closing the buffer stream. Must ask to save changes if the saved flaf is not true*/
	void q();

public:
	Led();								// ctor when there is no file
	Led(const string& fileName);		// ctor when a file is supplied
	void run();							// runs editing session until user quits
	// no more public methods here (we are limited to 3)
};
#endif