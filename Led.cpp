/*
Created by eugen on 5/28/2017
COMP 5421 :: Assignment 2
Eugen Caruntu #29077103
*/

#include <string>
#include <iostream>
#include <ostream>
#include <fstream>
#include <iterator>
#include <list>
#include "Led.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::iterator;
using std::ifstream;
using std::ofstream;
using::std::to_string;

/* default ctor */
Led::Led() :Led("") // delegating to parameterized constructor
{}

/* parameterized ctor */
Led::Led(const string& fileName) : fileName{ fileName }
{
	// make an input filestream
	ifstream fin(fileName);

	// check if file is opened
	if (!fin)
	{
		if (fileName == "") { cout << "\"?\" [NEW FILE]" << endl; }
		else { cout << "Unable to open file " << fileName << endl << "\"" << fileName << "\" [NEW FILE]" << endl; }
		saved = false;		// a new file might require saving
	}

	else
	{
		// add each line from the stream into buffer
		string line;
		while (getline(fin, line)) { buffer.push_back(line); }

		// close the stream
		fin.close();

		cout << this->fileName << " " << to_string(buffer.size()) << " lines" << endl;

		last_line = buffer.size() - 1;
		current_line = buffer.size() - 1;
	}
	cout << "Entering command mode." << endl;
	switchToCommand();
}

void Led::run()
{
	string command_line;
	Command cmd;
	do
	{
		getline(cin, command_line);							// read a command line
		cmd.parse(command_line, current_line, last_line);	// parse the command line and modify Command object's members
		execute(cmd);										// execute the command passing the updated Command object as argument
	} while (cmd.symbol != 'q');
}

void Led::execute(const Command& cmd)
{
	if (cmd.valid) {
		// call appropriate function
		switch (cmd.symbol) {
		case 'a': a(cmd.toRng); break;
		case 'i': i(cmd.toRng); break;
		case 'r': r(cmd.fromRng, cmd.toRng); break;
		case 'p': p(cmd.fromRng, cmd.toRng); break;
		case 'n': n(cmd.fromRng, cmd.toRng); break;
		case 'c': c(cmd.fromRng, cmd.toRng); break;
		case 'u': u(cmd.toRng); break;
		case 'd': d(cmd.toRng); break;
		case 'w': w(); break;
		case '=': equal(); break;
		case 'q': q(); break;
		}
	}
}

string Led::replace(string& s, const string& a, const string& b)
{
	if (s == "" || a == "") { return ""; }

	int i = 0; // start at position 0 of the string s
	while ((i = s.find(a, i)) != -1) // loop until not found anymore
	{
		s.replace(i, a.length(), b);
		i += b.length();
	}
	return s;
}

void Led::switchToCommand()
{
	cout << ": ";
}

void Led::advanceIterTo(list<string>::iterator& iter, const int pos)
{
	iter = buffer.begin();
	if (distance(iter, buffer.end()) >= pos) { advance(iter, pos); }// position iterator at y
}

void Led::a(const int y)
{
	i(y + 1); // delegate to i()
}

void Led::i(const int y)
{
	advanceIterTo(it, y);

	string line;
	getline(cin, line);
	while (cin.good() && line != ".")
	{
		buffer.insert(it, line);							// insert
		current_line = distance(buffer.begin(), it) - 1;	// gets the position of the iterator
		getline(cin, line);
	}

	last_line = buffer.size() - 1;
	saved = false;
	switchToCommand();
}

void Led::r(const int x, const int y)
{
	// position first iterator at x positions from begining
	advanceIterTo(it, x); 

	current_line = x;

	// position second iterator at y positions from begining
	advanceIterTo(itEnd, y);

	if (distance(it, itEnd) > 0) { it = buffer.erase(it, itEnd); }	// remove range
	if (distance(it, itEnd) == 0) { it = buffer.erase(it); }		// remove position
	if (buffer.size() == 0)											// if buffer remains empty add an empty line
	{
		buffer.insert(buffer.begin(), "");
		cout << "All lines were removed." << endl;
	}

	// set other data members
	last_line = buffer.size() - 1;
	current_line = (current_line > last_line) ? current_line-- : current_line;

	saved = false;
	p(current_line, current_line);
	//cout << ": ";  // only needed if p is not called in the line above
}

void Led::p(const int x, const int y)
{
	string lineNbr{ "" };
	// position first iterator at x positions from begining
	advanceIterTo(it, x);
	current_line = x;

	// position second iterator at y positions from begining
	advanceIterTo(itEnd, y);

	// print current line, if distance is 0 it wont loop, but ensure the buffer is not empty
	if (prtLines) { lineNbr = to_string(current_line + 1) + "\t"; }
	if (!buffer.empty()) { cout << lineNbr << *it << endl; }

	// print until iterators meet
	while (distance(it, itEnd) > 0)
	{
		it++;
		current_line++;
		if (prtLines) { lineNbr = to_string(current_line + 1) + "\t"; }
		if (!buffer.empty()) { cout << lineNbr << *it << endl; }
	}

	// turn off line numbers
	prtLines = false;

	cout << ": ";
}

void Led::n(const int x, const int y)
{
	prtLines = true;
	p(x, y); // delegate to p once turning on line numbers
}

void Led::c(const int x, const int y)
{
	string a{ "" };
	string b{ "" };
	cout << "change what ?\t";
	getline(cin, a);
	cout << "to what ?\t";
	getline(cin, b);

	// position first iterator at x positions from begining
	advanceIterTo(it, x); 
	current_line = x;

	// position second iterator at y positions from begining
	advanceIterTo(itEnd, y);

	// replace current line, if distance is 0 wont loop
	if (!buffer.empty()) { replace(*it, a, b); }

	// continue and replace until iterators meet
	while (distance(it, itEnd) > 0)
	{
		it++;
		current_line++;
		if (!buffer.empty()) { replace(*it, a, b); }
	}

	saved = false;
	cout << ": ";
}

void Led::u(const int y) // y is distance
{
	int targetDest = current_line - y;
	if (targetDest < 0) { cout << "BOF reached" << endl; p(current_line, current_line); } // stay at current_line if out of range
	else if (targetDest >= 0) { p(targetDest, targetDest); }	// moves the current_line by y positions and prints new current_line
}

void Led::d(const int y) // y is distance
{
	int targetDest = current_line + y;
	if (targetDest > last_line) { cout << "EOF reached" << endl; p(current_line, current_line); }  // stay at current_line if out of range
	else if (targetDest <= last_line) { p(targetDest, targetDest); }	// moves the current_line by y positions and prints new current_line
}

void Led::w()
{
	// asks for fileName if empty
	if (fileName == "")
	{
		do {
			cout << "Specify the name of the file. If the file exists it will be overwritten: " << endl;
			getline(cin, fileName);
		} while (cin.bad() || fileName == "");
	}

	// make a output filestream
	ofstream fout(fileName);

	// check if file is opened
	if (!fout)
	{
		cout << "Cound not open the file for writing: " << fileName << endl;
		exit(1);
	}

	//iterate through the buffer and write out
	if (!buffer.empty()) {
		for (it = buffer.begin(); it != buffer.end(); ++it)
		{
			fout << *it << '\n';
		}
	}

	// close the stream
	fout.close();

	saved = true;
	cout << "\"" << fileName << "\" " << buffer.size() << " lines written" << endl << ": ";
}

void Led::equal()
{
	cout << current_line + 1 << endl << ": "; // we need to increment since list counts from 0
}

void Led::q()
{
	if (!saved)
	{
		string answer;
		do {
			cout << "Save changes to \"" << fileName << "\"  (y / n) ? ";
			getline(cin, answer);
			if (!cin || cin.bad() || !(answer == "y" || answer == "n"))
			{
				cin.clear();
				cout << "Invalid answer: " << answer << endl;
				cout << "enter 'y' for yes and 'n' for no." << endl;
			}
		} while (!(answer == "y" || answer == "n"));

		if (answer == "y") { w(); }; // write out if 'y'
	}

	cout << "quitting led. ...bye." << endl;

}
