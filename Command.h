/*
Created by eugen on 5/28/2017
COMP 5421 :: Assignment 2
Eugen Caruntu #29077103
*/

#ifndef COMMAND_H
#define COMMAND_H
#include <string>

using std::string;

class Command
{
private:
	string commands{ "airpncudw=q" };	// a string containing all possible commands
	int fromRng{ -1 };					// fromRng of command's line range
	int toRng{ -1 };					// toRng of command's line range
	bool valid{ false };				// qualifier flag for command once parsed
	char symbol{ '\0' };				// the command itself

	string removeSpaces(string str);					// helper method to remove white spaces from a string
	int intConvert(const string& str) const;			// custom helper method to convert strings to integers
	void decrement();									// decrement before returning to LED
	friend class Led;
	
public:
	Command();																	// default ctor
	void parse(const string& cmdLine, const int fromRng, const int toRng);		// parse a command line

};
#endif