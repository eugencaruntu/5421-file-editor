COMP 5421 :: Assignment 2
Eugen Caruntu #29077103

DELIVERY NOTES:
There were some minor contradictions in instructions:
1. d command as used on page 12 (line 55) should not imediately return EOF since d would set current_line at position of last inserted line (i used on line 43 would insert a few lines BEFORE line 1).
2. d and u when reaching EOF/BOF are inconsistently displaying the current_line in addition to EOF/BOF (page 18 line 347-348 vs. page 12 line 56)
3. r would remove line(s), but does it need to print current_line once removed? (page 4 line 32-33 vs. page 14 line 116)
4. q does not mention if is required to save a [NEW] file when no edits were done on this new file (or a named file that was not found and opened as new)
Assumptions:
1. usage of d is incorrect on page 12 of the PDF
2. when reaching EOF/BOF the current_line adjacnet to EOF/BOF will also be printed 
3. r will also print the current_line after removal. In case when all lines are removed, a message will indicate that to user
4. q will ask to save a [NEW] file if no edits were done
<EOF>